﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class beaconList : MonoBehaviour
{
    // Use this for initialization
    Dictionary<string,Beacon> beaconDict = new Dictionary<string, Beacon>();
    public Text txt;

    void Start()
    {
        iBeaconReceiver.Scan();
        iBeaconReceiver.BeaconRangeChangedEvent += beaconChanged;
    }

    // Update is called once per frame
    void Update()
    {
        //txt.text = "N/A";
    }
    void beaconChanged(Beacon[] beacons)
    {
        if (beacons[0] == null)
        {
			txt.text = "N/A";
        }
        else
        {
            foreach (Beacon bcs in beacons)
            {
                string key = string.Format("{0}.{1}.{2}", bcs.UUID, bcs.major, bcs.minor);
                if(beaconDict.ContainsKey(key))
                {
                    beaconDict[key] = bcs;
                }
                else
                {
                    beaconDict.Add(key, bcs);
                }
            }
            txt.text = beaconDict.Count + "\n";
            foreach(var bcs in beaconDict.Values.OrderBy(b => b.accuracy))
            {
                //Debug.Log("UUID: "+bcs.UUID+" / Major"+bcs.major+" / Minor"+bcs.minor+" / Strength"+bcs.strength);
                //txt.text += "Major"+bcs.major+" / Minor"+bcs.minor+" / Strength"+bcs.strength +"\n";
                
                txt.text += bcs.regionName + " / Range" + bcs.accuracy + "\n";
            }
        }
    }
}
